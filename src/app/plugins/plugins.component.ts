import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'plugins-component',
    template: '<div><span> IN SECTION PLUGINS </span></div>'
})
export class PluginsComponent implements OnInit {

    constructor(public router: Router) { }

    ngOnInit() {
		console.log(this.router.url);
    }

}