import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PluginsRoutingModule } from './plugins-routing.module';
import { PluginsComponent } from './plugins.component';

@NgModule({
	imports:[
		CommonModule,
		PluginsRoutingModule
	],
	declarations: [
		PluginsComponent
	]
})
export class PluginsModule {}