import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FlowChartsComponent } from './flow-charts.component';

const routes: Routes = [
	{path:'',component: FlowChartsComponent}
];
@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class FlowChartsRoutingModule { }