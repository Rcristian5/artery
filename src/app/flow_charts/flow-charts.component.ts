import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'flow-charts-component',
    template: '<div><span> IN SECTION FLOW CHARTS</span></div>'
})
export class FlowChartsComponent implements OnInit {

    constructor(public router: Router) { }

    ngOnInit() {
		console.log(this.router.url);
    }

}