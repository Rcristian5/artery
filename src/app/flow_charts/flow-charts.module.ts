import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FlowChartsRoutingModule } from './flow-charts-routing.module';
import { FlowChartsComponent } from './flow-charts.component';

@NgModule({
	imports:[
		CommonModule,
		FlowChartsRoutingModule
	],
	declarations: [
		FlowChartsComponent
	]
})
export class FlowChartsModule {}