import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { } from '';

const routes : Routes = [
	{path: '', loadChildren: './common/commons.module#CommonsModule'},
	// {path: 'login', loadChildren: ''},
	// {path: 'signup', loadChildren: ''},
	// {path: 'not-found', loadChildren: ''},
	{path: '**', redirectTo: 'flow-charts'},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }