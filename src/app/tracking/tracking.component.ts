import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'tracking-component',
    template: '<div><span> IN SECTION TRAKING </span></div>'
})
export class TrackingComponent implements OnInit {

    constructor(public router: Router) { }

    ngOnInit() {
		console.log(this.router.url);
    }

}