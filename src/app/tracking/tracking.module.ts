import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrackingRoutingModule } from './tracking-routing.module';
import { TrackingComponent } from './tracking.component';

@NgModule({
	imports:[
		CommonModule,
		TrackingRoutingModule
	],
	declarations: [
		TrackingComponent
	]
})
export class TrackingModule {}