import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout.component';
const routes: Routes = [
	{
		path: '',
		component: LayoutComponent,
		children: [
			{path:'flow-charts',loadChildren: '../flow_charts/flow-charts.module#FlowChartsModule'},
			{path:'plugins',loadChildren: '../plugins/plugins.module#PluginsModule'},
			{path:'tracking',loadChildren: '../tracking/tracking.module#TrackingModule'}
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class CommonsRoutingModule {}
