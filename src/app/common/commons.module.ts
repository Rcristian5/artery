import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { CommonsRoutingModule } from './commons-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';

@NgModule({
	imports: [
		CommonModule,
		NgbDropdownModule.forRoot(),
		CommonsRoutingModule
	],
	declarations: [
		LayoutComponent,
		HeaderComponent,
		SidebarComponent
	]
})
export class CommonsModule {}