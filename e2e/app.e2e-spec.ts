import { Artery2.0Page } from './app.po';

describe('artery2.0 App', () => {
  let page: Artery2.0Page;

  beforeEach(() => {
    page = new Artery2.0Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
